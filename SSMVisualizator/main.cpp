/*
* SSMVisualization
* Copyright (c) 2016 Anton Mikhailov (Zifi2)
* https://bitbucket.org/zifi2/smmvisualizator
*/

#pragma comment( lib, "freeglut.lib" )

#pragma comment(lib,"DevIL.lib")
#pragma comment(lib,"ilut.lib")
#pragma comment(lib,"ilu.lib")

#include "main.h"

/*FRAMERATE*/
int framerate=120;
int msForFrame = 1000/framerate;

/*CAMERA*/
bool rightButton = false;
float camX = 120.0f, camY = 120.0f, camZ = 120.0f;
float camDirectionX = 0.0f, camDirectionY = 0.0f, camDirectionZ = 0.0f;
float angleX = 0.0f, angleY = 0.0f, angleZ = 0.0f;
float angleOldX = 0.0f, angleOldY = 0.0f, angleOldZ = 0.0f;
float moveStep = 10.0f;//1.0f;
float angleStep = 0.3f;
float viewMatrix[16];///

/*COORDINATE AXIS*/
float red=1.0f, blue=1.0f, green=1.0f;// ��� X,Y,Z

/*TIME*/
int timeScale = 1;//������� �������� ������ �� ���� ����(��������� ������������ �������)
int timeStep = 1; //�� ������� �� ��������� ������� �������� ���� ����(��������� ��������� �������)
unsigned int seconds = 0;//������� ����� �������������
unsigned int periodOfSunRotation = 31536000;
unsigned int periodOfEarthRotation = 86400;
const int date = 31101994;
 
/*TEXTURES*/
GLuint earth,space,moon,clouds,sun,star,atmos,earth_night;

/*PARAMETERS OF OBJECTS*/
float degree, degree_clouds, degree_sun;

/*READING FROM FILE*/
std::string str;
std::ifstream file(L"data/stars.cat",std::ios::in); 
std::vector<std::string> v;
std::vector<Star_spr> v_stars;
float catalog_distance_scale = 100;

/*PARAMETERS OF VISUALISATION*/
bool display_coordinate_axis = true;
bool display_star = false;
bool load_star_catalog = false;
bool smooth = false;
const float max_distance = 1000.00f*2.0f;//400.0f; 92950000.0f;
const float star_height = 0.0125f*max_distance;//5.0f;
const float star_weight = star_height;

float pitchEarthToSun = 23.45f;

float earthSize = 63.78f;//30.0f;  6378.0f;
float cloudsSize = earthSize + 0.5f;//65.0f;//30.2f;  6600.0f;///???
float moonSize = 4.51f; //10.0f;  1737.0f;
float sunSize = moonSize*2.0f;//25.0f; 695500.0f;
float moonDistance = 1000.00f;//384467.0f; //100.0f;
float moonConstRotate = -60.0f;
float atmosSize = earthSize+6.0f;//70.78f;

float lineWidth = 3.0f;
float axisLength = 10.0f;

GLUquadricObj *quadObjSpace;
GLUquadricObj *quadObjEarth;
GLUquadricObj *quadObjClouds;
GLUquadricObj *quadObjMoon;
GLUquadricObj *quadObjAtmos;

objModel tempobjModel;

/*LIGHTING*/
float light0_diffuse[] = {1.0f, 1.0f, 1.0f,0.0f};
float light0_direction[] = {0.0f, 0.0f, 0.0f, 1.0f};

/*GUI*/
int cursor=0;
 
/*Model position */
float horizontal_angle = 0;
float vertical_angle = 0;
float x=100.0f, y=1.0f, z=1.0f;
float sx=0.0f, sy=0.0f, sz=0.0f;

void split(const std::string& s, char c, std::vector<std::string>& v) { ///???
   std::string::size_type i = 0;
   std::string::size_type j = s.find(c);
   v.reserve(10000);
   while (j != std::string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);

      if (j == std::string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

void loadStarCatalog(){//??? ��������������
	glutSetCursor(GLUT_CURSOR_WAIT);
	//std::ios::sync_with_stdio(false);
	while(getline(file, str))
	file.close();
	/*while(!file.eof())
   {
    getline(file, str);
    //std::cout<<str<<std::endl;
   };*/
	 split(str,';',v);
   //for (unsigned int i = 0; i < v.size(); ++i) {
   //   std::cout << v[i] << '\n';
   //}
	//v_stars.reserve(10000);
	for (unsigned int j = 0; j < v.size(); j+=7) {
	v_stars.push_back(Star_spr(std::stof(v[j]), std::stof(v[j+1]), std::stof(v[j+2]),
								std::stof(v[j+3])*catalog_distance_scale, std::stof(v[j+4])*catalog_distance_scale,
								std::stof(v[j+5])*catalog_distance_scale,v[j+6]));

	//cout << v_stars.back();
	glutSetCursor(GLUT_CURSOR_INHERIT);
	}
}

void displayStar(){////??? ��������������
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, star);
	glPolygonMode(GL_FRONT_AND_BACK,  GL_FILL);

	for (unsigned int j = 0; j < v_stars.size(); ++j) {
	//	cout << v[j] << "\n";
		glPushMatrix();
		//cout << test;
		glRotatef(v_stars[j].ra, 0.0f, 0.0f, 1.0f);//������� ����������� � ��������� XY
		glRotatef(v_stars[j].dec, 0.0f, -1.0f, 0.0f);
		glTranslatef(max_distance,0.0f,0.0f);
		renderBitmapString(0.0f, 0.0f, earthSize*1.2f, GLUT_BITMAP_TIMES_ROMAN_24, "Earth");//!

			glBegin(GL_TRIANGLE_FAN);
			glColor4f(v_stars[j].r_color,v_stars[j].g_color,v_stars[j].b_color,1.0f);
			glTexCoord3f(0, 0, 0);		glVertex3f(0.0f,-star_weight,-star_height);
			glTexCoord3f(1, 0, 0);		glVertex3f(0.0f,-star_weight,star_height);
			glTexCoord3f(1, 1, 0);		glVertex3f(0.0f,star_weight,star_height);
			glTexCoord3f(0, 1, 0);		glVertex3f(0.0f,star_weight,-star_height);
			glEnd();
		glPopMatrix();
	}
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_BLEND);
	glDisable( GL_TEXTURE_2D);	
}

void loadTextures(){ // todo ����������

	earth = ilutGLLoadImage("textures/earth.jpg");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//������� image

	space = ilutGLLoadImage("textures/milkyway.jpg");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	moon = ilutGLLoadImage("textures/moon.jpg");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	clouds = ilutGLLoadImage("textures/clouds.png");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	sun = ilutGLLoadImage("textures/sun.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	star = ilutGLLoadImage("textures/star.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	atmos = ilutGLLoadImage("textures/Messenger_tex.png");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	earth_night = ilutGLLoadImage("textures/earth_night.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void reshapeScene(int width, int height) {
	// ����������� ������� �� ����
	if(height == 0)	height = 1;
	float ratio = 1.0f * width / height;
	// ���������� ������� ��������
	glMatrixMode(GL_PROJECTION);
    // Reset �������
	glLoadIdentity();
	// ���������� ���� ���������
	glViewport(0, 0, width, height);
	// ���������� ���������� �����������.
	gluPerspective(55,ratio,1,max_distance*2);
	// ��������� � ������
	glMatrixMode(GL_MODELVIEW);
}

void processNormalKeys(unsigned char key, int x, int y) {
	switch (key) {
		case 27 :
			exit(0);
		break;

		case 'w' :
			camDirectionZ += moveStep;
		break;
		case 246 : //�
			camDirectionZ += moveStep;
		break;

		case 's' :
			camDirectionZ -= moveStep;
		break;
		case 251 : //�
			camDirectionZ -= moveStep;
		break;

		case 43 : //+
			if(timeScale <= 0) {timeScale=1;break;}
			if(timeScale >= INT_MAX/10) {timeScale=INT_MAX;break;}
			timeScale=timeScale*10;
		break;

		case 45 : //-
			timeScale=timeScale/10;
		break;
	}
		cout << (int) key << " timescale:" << timeScale << "\n";
}

void processSpecialKeys(int key, int x, int y) {
 	switch (key) {
		case GLUT_KEY_LEFT :
				angleZ += angleStep;
			break;

		case GLUT_KEY_RIGHT :
				angleZ -= angleStep;
			break;

		case GLUT_KEY_UP :
				if((pow(camX,2)+pow(camY,2)+pow(camZ,2))>moveStep){//�������� moveStep �� ������ �����
					camX -= moveStep;
					camY -= moveStep;
					camZ -= moveStep;
				}
			break;

		case GLUT_KEY_DOWN :
					camX += moveStep;
					camY += moveStep;
					camZ += moveStep;
			break;

		case GLUT_KEY_HOME :
			camX = 100.0f, camY = 100.0f, camZ = 100.0f;
			camDirectionX = 0.0f, camDirectionY = 0.0f, camDirectionZ = 0.0f;
			break;

		case GLUT_KEY_F2 :
			if(v_stars.size()<=0){
				loadStarCatalog(); 
				load_star_catalog = true;
			}
			display_star=!display_star;
			break;

		case GLUT_KEY_F3 :
			smooth=!smooth;
			ilutGLScreen();
			ilSave( IL_JPG, "screens/Screen.jpg" );
			//	ilutGLScreenie();
			break;
	}
}

/*MOUSE ROTATION*/
void mouseButton(int button, int state, int x, int y) {
	// only start motion if the left button is pressed
	if (button == GLUT_RIGHT_BUTTON) {
 
		// when the button is released
		if (state == GLUT_UP) {
			glutSetCursor(GLUT_CURSOR_INHERIT);
			rightButton = false;
		}
		else  {// ����� ���������� ��� ���������� ��� �����
			glutSetCursor(GLUT_CURSOR_CYCLE);
			angleOldZ = (float) x;
			angleOldX = (float) y;
			rightButton = true;
		}
	}
}

void mouseMove(int x, int y) { 	
         // ������ ��� ��������� ���������� �������� ���������� � �������� �������
	if (rightButton == true) {
			angleZ += angleStep*(x-angleOldZ);
			angleOldZ = (float) x;
			angleX += angleStep*(y-angleOldX);
			angleOldX = (float) y;
	}
}

void renderBitmapString(float x,float y,float z,void *font,char *string) {
  char *c;
  glRasterPos3f(x, y, z);	
  for (c=string; *c != '\0'; c++) {
	glutBitmapCharacter(font,*c);
  }
}

void displaySphere(int textureBindID, GLUquadricObj* quadricObj, double radius, int slices, int stacks, bool blend){

	glEnable( GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	if(blend) glEnable(GL_BLEND);
	if(blend) glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, textureBindID);
		gluSphere(quadricObj, radius, slices, stacks);//������� ��������
	if(blend)glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
	glDisable( GL_TEXTURE_2D);
}

void displaySpace(){
	glEnable( GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, space);
		gluSphere(quadObjSpace, max_distance, 40, 40);
	glDisable( GL_TEXTURE_2D);
}

void displayCoordinateAxis(float lineWidth, float axisLength){
	glLineWidth(lineWidth);
	 glBegin(GL_LINES);
		  glColor3f(1,0,0);     
		  glVertex3f(0,0,0); 
		  glVertex3f(axisLength,0,0);
		  glColor3f(0,1,0);     
		  glVertex3f(0,0,0); 
		  glVertex3f(0,axisLength,0);
		  glColor3f(0,0,1);    
		  glVertex3f(0,0,0);
		  glVertex3f(0,0,axisLength);
		  glGetFloatv(GL_MODELVIEW_MATRIX, viewMatrix);
		  glColor3f(0.5f,0.5f,0.5f);    
		  glVertex3f(0,0,0);
		  glVertex3f(viewMatrix[1]*10,viewMatrix[5]*10,viewMatrix[9]*10);
	 glEnd();
}

void displaySprite(int textureBindID, float spriteSize){
	glEnable( GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, textureBindID);
		glPolygonMode(GL_FRONT_AND_BACK,  GL_FILL);
		glGetFloatv(GL_MODELVIEW_MATRIX, viewMatrix);
		glBegin(GL_TRIANGLE_FAN); 
		float p1 [3] = {(viewMatrix[1]-viewMatrix[0])*spriteSize,(viewMatrix[5]-viewMatrix[4])*spriteSize,(viewMatrix[9]-viewMatrix[8])*spriteSize};
		float p2 [3] = {(-viewMatrix[1]-viewMatrix[0])*spriteSize,(-viewMatrix[5]-viewMatrix[4])*spriteSize,(-viewMatrix[9]-viewMatrix[8])*spriteSize};
		float p3 [3] = {(-viewMatrix[1]+viewMatrix[0])*spriteSize,(-viewMatrix[5]+viewMatrix[4])*spriteSize,(-viewMatrix[9]+viewMatrix[8])*spriteSize};
		float p4 [3] = {(viewMatrix[1]+viewMatrix[0])*spriteSize,(viewMatrix[5]+viewMatrix[4])*spriteSize,(viewMatrix[9]+viewMatrix[8])*spriteSize};
		glTexCoord3f(1, 1, 0);	glVertex3fv(p1);
		glTexCoord3f(0, 1, 0);	glVertex3fv(p2);
		glTexCoord3f(0, 0, 0);	glVertex3fv(p3);
		glTexCoord3f(1, 0, 0);	glVertex3fv(p4);
		glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
	glDisable( GL_TEXTURE_2D);
}

void displayScene(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ������ ����� � �������
	glLoadIdentity(); // ��������� �������������
	
	/*CAMERA AND MOUSE MOVING*/
	gluLookAt(camX, camY, camZ,
			  camDirectionX, camDirectionY, camDirectionZ,
			  0.0f, 0.0f, 1.0f);
	glGetFloatv(GL_MODELVIEW_MATRIX, viewMatrix);
	float upVecX[3]={viewMatrix[1],viewMatrix[5],viewMatrix[9]};
	float rightVecX[3]={viewMatrix[0],viewMatrix[4],viewMatrix[8]};
	glRotatef(-angleZ, upVecX[0], upVecX[1], upVecX[2]);//������� ����������� � ��������� XY
	glRotatef(-angleX, rightVecX[0], rightVecX[1], rightVecX[2]);// � ��������� YZ
	//������� ������ ��� Y �� ������������
	
	glColor3f(0.8f, 0.8f, 0.8f);

	if(smooth){
		glEnable(GL_POINT_SMOOTH);    // ����������� �����
		glEnable(GL_LINE_SMOOTH);    // ����������� �����
		glEnable(GL_POLYGON_SMOOTH);    // ����������� ����� ���������������
	}

	displaySpace();
	
	glEnable(GL_CULL_FACE);
	if(display_star) displayStar();
	glDisable(GL_CULL_FACE);

	glPushMatrix();//Sun sprite
		//cout << pred;
		degree_sun = (float)(seconds % periodOfSunRotation)*360/periodOfSunRotation; 
		glRotatef(degree_sun, 0.0f, 0.0f, 1.0f);
		glRotatef(pitchEarthToSun, 0.0f, 1.0f, 0.0f);
		glTranslatef(max_distance,0.0f,0.0f);
		glColor4f(1.0f,1.0f,1.0f,1.0f);
		displaySprite(sun,sunSize);
		char ch_degree_sun[32];//!
		sprintf_s(ch_degree_sun,"%f",degree_sun);//!
		renderBitmapString(0.0f, 0.0f, sunSize*2, GLUT_BITMAP_TIMES_ROMAN_24, ch_degree_sun);//!
		renderBitmapString(0.0f, 0.0f, sunSize*8, GLUT_BITMAP_TIMES_ROMAN_24, "Sun");//!

		glEnable(GL_LIGHTING); // ����� ���������� ������ ��������� 
	    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE); // ������ ���, ����� ���������� ��� ������� ��������
	    glEnable(GL_NORMALIZE); //����� ������� ���������� �������� �� ��������� ����������

	    glEnable(GL_LIGHT0); // ��������� ������������ light0
		glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse); // ������������� ��������� ����� light0 ��������� ����, ������� ������� ����� 
	    glLightfv(GL_LIGHT0, GL_POSITION, light0_direction); // ������������� ����������� ��������� �����, ��������� �����
	glPopMatrix();

	glPushMatrix();
		glRotatef(degree_sun+180.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(pitchEarthToSun, 0.0f, 1.0f, 0.0f);
		glTranslatef(max_distance,0.0f,0.0f);
		glEnable(GL_LIGHT1); // ��������� ������������ light0
		glLightfv(GL_LIGHT1, GL_DIFFUSE, light0_diffuse); // ������������� ��������� ����� light0 ��������� ����, ������� ������� ����� 
	    glLightfv(GL_LIGHT1, GL_POSITION, light0_direction); // ������������� ����������� ��������� �����, ��������� �����
		glDisable(GL_LIGHT1);
	glPopMatrix();

   glEnable(GL_DEPTH_TEST);
	glPushMatrix();// Earth
		//������ �����
		glColor3f(1.0f, 1.0f, 1.0f);
		//� ����������� �� ������� ������� ���������� ������ �������� ����� �� 0 �� 360
		degree = (float)(seconds % periodOfEarthRotation)*360/periodOfEarthRotation;  
		glRotatef(degree, 0.0f, 0.0f, 1.0f);//�������� ����� ������ ����� ���
		glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
			displaySphere(earth,quadObjEarth,earthSize, 40, 40, false);
		glDisable(GL_LIGHT0);
		glEnable(GL_LIGHT1);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
			displaySphere(earth_night,quadObjEarth,earthSize, 40, 40, true);
		glDisable(GL_CULL_FACE);
	    glEnable(GL_DEPTH_TEST);
		glDisable(GL_LIGHT1);
		glEnable(GL_LIGHT0);
		glPushMatrix();//clouds
			/*degree_clouds = (float)(seconds % 86400)*360/86400;
			glRotatef(degree_clouds, 0.0f, 0.0f, 1.0f);*/
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			displaySphere(clouds,quadObjClouds,cloudsSize, 40, 40,true);
		glPopMatrix();//clouds
		glDisable(GL_LIGHTING);
			renderBitmapString(0.0f, 0.0f, earthSize*1.2f, GLUT_BITMAP_TIMES_ROMAN_24, "Earth");//!
			char ch_seconds[32];//!
			sprintf_s(ch_seconds,"%d",seconds);//!
			//renderBitmapString(0.0f, 0.0f, earthSize+1.3f, GLUT_BITMAP_TIMES_ROMAN_24, ch_seconds);//!
			char ch_degree[32];//!
			sprintf_s(ch_degree,"%f",degree);//!
			//renderBitmapString(0.0f, 0.0f, earthSize+1.4f, GLUT_BITMAP_TIMES_ROMAN_24, ch_degree);//!
		glEnable(GL_LIGHTING);
		glPushMatrix();//Moon
			glTranslatef(moonDistance,0.0f,0.0f);///!
			glRotatef(moonConstRotate, 0.0f, 0.0f, 0.5f);//������������ ����, ����� ������� �������� �� ����///!
			//displaySphere(moon,quadObjClouds,moonSize, 20, 20,false);
			renderBitmapString(0.0f, 0.0f, moonSize*1.2f,GLUT_BITMAP_TIMES_ROMAN_24, "Moon");//!
		glPopMatrix();//Moon
	glPopMatrix();//Earth

	glPushMatrix();//obj
		x = 80 * cos(seconds*0.001f);
		y = 80 * sin(seconds*0.001f);
		glTranslatef(x,y,z);///!
			//glDisable(GL_LIGHTING);
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glEnable( GL_TEXTURE_2D);
			//glEnable(GL_ALPHA_TEST);
			//glEnable(GL_BLEND);
			//glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
			//glEnable(GL_CULL_FACE);
			glBindTexture(GL_TEXTURE_2D, atmos);
			glRotatef(90, 0.0f, 0.0f, 1.0f);
			//glPolygonMode(GL_FRONT,  GL_FILL);
			tempobjModel.display(1.0f);		
			glDisable(GL_BLEND);
			glDisable(GL_ALPHA_TEST);
			glDisable( GL_TEXTURE_2D);
			//glDisable(GL_CULL_FACE);
		glDisable(GL_LIGHTING);
		renderBitmapString(0.0f, 0.0f, moonSize*1.2f,GLUT_BITMAP_TIMES_ROMAN_24, "obj");//!
	glPopMatrix();//obj
	for(unsigned i=0; i<20; i++){
	glPushMatrix();//obj
		x = (60+i*10) * cos(seconds*0.01f*i);
		y = (60+i*10) * sin(seconds*0.01f*i);
		glTranslatef(x,y,z);///!
			//glDisable(GL_LIGHTING);
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glEnable( GL_TEXTURE_2D);
			//glEnable(GL_ALPHA_TEST);
			//glEnable(GL_BLEND);
			//glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
			//glEnable(GL_CULL_FACE);
			glBindTexture(GL_TEXTURE_2D, atmos);
			glRotatef(90, 0.0f, 0.0f, 1.0f);
			//glPolygonMode(GL_FRONT,  GL_FILL);
			tempobjModel.display(1.0f);		
			glDisable(GL_BLEND);
			glDisable(GL_ALPHA_TEST);
			glDisable( GL_TEXTURE_2D);
			//glDisable(GL_CULL_FACE);
		glDisable(GL_LIGHTING);
		renderBitmapString(0.0f, 0.0f, moonSize*1.2f,GLUT_BITMAP_TIMES_ROMAN_24, "obj");//!
	glPopMatrix();//obj
	}

  //glDisable ( GL_DEPTH_TEST );
  //glMatrixMode(GL_PROJECTION);
  //glPushMatrix();
  ////glLoadIdentity();
  //gluOrtho2D(100, 500, 100, 500);
  //glMatrixMode(GL_MODELVIEW);
  //glPushMatrix();
  //glLoadIdentity();
  //displaySphere(earth,quadObjEarth,earthSize, 0, 0, false);
  //displayCoordinateAxis(lineWidth,axisLength);
  //glPopMatrix();
  //glMatrixMode(GL_PROJECTION);
  //glPopMatrix();
  //glMatrixMode(GL_MODELVIEW);
  //glEnable (GL_DEPTH_TEST);

	//���� ������ �� ��������
	//glPushMatrix();
	//	glGetFloatv(GL_MODELVIEW_MATRIX, viewMatrix);
	//	glScalef(0.1f,0.1f,0.1f);
	//	float upX=viewMatrix[1],upY=viewMatrix[5],upZ=viewMatrix[9];
	//	float c2X=viewMatrix[0],c2Y=viewMatrix[4],c2Z=viewMatrix[8];
	//	float modCam = sqrt(pow(upX,2)+pow(upY,2)+pow(upZ,2));
	//	float modCam2 = sqrt(pow(c2X,2)+pow(c2Y,2)+pow(c2Z,2));
	//	float degreeAtmosXOZ = acos(upZ/(modCam))*180/3.1415f;
	//	//printf("%f,%f,%f,\t%f\n",upX,upY,upZ /(1+modCam));
	//	float degreeAtmosXOY = acos(c2Y/(modCam2))*180/3.1415f;
	//	//glTranslatef(moonDistance,0.0f,0.0f);///!
	//	cout << upX << ";" << upY << ";" << upZ << ";" << modCam << ";" << modCam2 << ";" << degreeAtmosXOZ << ";" << degreeAtmosXOY << "\n";
	//	 glDisable( GL_DEPTH_TEST);
	//	 glLineWidth(lineWidth);
	//	 glBegin(GL_LINES);
	//	  glColor3f(0,1,0); 
	//	  glVertex3f(0,0,0);
	//	 glVertex3f(c2X,c2Y,c2Z);
	//	  glColor3f(0.5f,0.5f,0.5f);    
	//	  glVertex3f(0,0,0);
	//	  glVertex3f(upX,upY,upZ);
	// glEnd();
	//  glEnable( GL_DEPTH_TEST);
	//glRotatef(degreeAtmosXOZ+45, 0.0f, 0.0f, -1.0f);//������� ����������� � ��������� XY
	//glRotatef(degreeAtmosXOY+45.0f, 0.0f, 1.0f, 0.0f);// � ��������� YZ
	//	displaySphere(atmos,quadObjAtmos,cloudsSize+10.0f+0.1f, 120, 120,true);
	//	   glDisable(GL_LIGHTING);
	//	   glDisable( GL_DEPTH_TEST);
	//		glBegin(GL_LINES);
	//	glLineWidth(lineWidth);
	//	  glColor3f(1,0.5,0);     
	//	  glVertex3f(0,0,0); 
	//	  glVertex3f(upX,upY,upZ);
	//	  glColor3f(0,1,1);     
	//	  glVertex3f(0,0,0); 
	//	  glVertex3f(c2X,c2Y,c2Z);
	// glEnd();
	//glPopMatrix();

   glDisable( GL_DEPTH_TEST); //��������� ����� �������
   glDisable(GL_LIGHT0); 
   glDisable(GL_LIGHTING);

	if(display_coordinate_axis) displayCoordinateAxis(lineWidth,axisLength);
	//ilutGLScreenie();

	if(smooth){
		glDisable(GL_POINT_SMOOTH);   // ����������� �����
		glDisable(GL_LINE_SMOOTH);    // ����������� �����
		glDisable(GL_POLYGON_SMOOTH); // ����������� ����� ���������������
	}

	glutSwapBuffers();
}

void fpsCheck(int timerid) { // �������� ������ glut
   glutTimerFunc(msForFrame, fpsCheck, timerid); // �������������� ������
   glutPostRedisplay(); // ������������
}

void timeIncr(int timerid) {
	seconds+=timeScale;
	glutTimerFunc(timeStep, timeIncr, 0);
}

int main (int argc, char * argv[]){
	// ������������� glut
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
	glutInitWindowSize(1024, 768);
	glutInitWindowPosition(200, 200); //hardcode
	glutCreateWindow("SSMVisualization");//todo GameMode

		/*DevIL init*/
	if ( ilGetInteger ( IL_VERSION_NUM ) < IL_VERSION ){
		fprintf ( stderr, "Incorrect devil.dll version\n" );
		return 0;
	}

	if ( iluGetInteger ( ILU_VERSION_NUM ) < ILU_VERSION ){
		fprintf ( stderr, "Incorrect ilu.dll version\n" );
		return 0;
	}

	if ( ilutGetInteger ( ILUT_VERSION_NUM ) < ILUT_VERSION ){
		fprintf ( stderr, "Incorrect ilut.dll version\n" );
		return 0;
	}

	ilInit   ();
	iluInit  ();
	ilutInit ();

	ilutRenderer ( ILUT_OPENGL );
	ilSetInteger ( IL_KEEP_DXTC_DATA, IL_TRUE );
	ilutEnable   ( ILUT_GL_AUTODETECT_TEXTURE_TARGET );
	ilutEnable   ( ILUT_OPENGL_CONV );
	ilutEnable   ( ILUT_GL_USE_S3TC );

	if(load_star_catalog) loadStarCatalog();//�������� ��������� ��������
	loadTextures();//�������� �������

	/*����������� �������*/
	glutReshapeFunc(reshapeScene);
	glutDisplayFunc(displayScene);
	glutTimerFunc(msForFrame, fpsCheck, 0);
	glutTimerFunc(timeStep, timeIncr, 0);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMove);

	/*quadric Obj init and setting*/
	quadObjSpace = gluNewQuadric();///
	gluQuadricTexture(quadObjSpace, GL_TRUE);///
	gluQuadricNormals(quadObjSpace, GLU_SMOOTH);
	gluQuadricDrawStyle(quadObjSpace, GLU_FILL);///

	quadObjEarth = gluNewQuadric();///
	gluQuadricTexture(quadObjEarth, GL_TRUE);//////
	gluQuadricNormals(quadObjEarth, GLU_SMOOTH);
	gluQuadricDrawStyle(quadObjEarth, GLU_FILL);///

	quadObjClouds = gluNewQuadric();///
	gluQuadricTexture(quadObjClouds, GL_TRUE);//////
	gluQuadricNormals(quadObjClouds, GLU_SMOOTH);
	gluQuadricDrawStyle(quadObjClouds, GLU_FILL);///

	quadObjMoon = gluNewQuadric();
	gluQuadricTexture(quadObjMoon, GL_TRUE);
	gluQuadricNormals(quadObjMoon, GLU_SMOOTH);
	gluQuadricDrawStyle(quadObjMoon, GLU_FILL);

	quadObjAtmos = gluNewQuadric();///
	gluQuadricTexture(quadObjAtmos, GL_TRUE);//////
	gluQuadricNormals(quadObjAtmos, GLU_SMOOTH);
	gluQuadricDrawStyle(quadObjAtmos, GLU_FILL);///

	tempobjModel.loadObjModel("data/test.obj");

	glutMainLoop();
	
	return 0;
} 