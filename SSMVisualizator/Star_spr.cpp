#pragma once

#include <string>
#include <math.h>
#include <iostream>
#include <sstream>

using namespace std;
class Star_spr{
public:
//private:
	std::string spectr;
	float	ra,
			dec,
			magn,
			x,
			y,
			z,
			distance,
			size,
			r_color,
			g_color,
			b_color;
//public:
	Star_spr(float ra, float dec, float magn, float x, float  y, float z, std::string spectr):
		ra(ra), dec(dec), magn(magn), x(x), y(y), z(z), spectr(spectr){
		//float dx =  pow(x,2);
		//float dy =  pow(y,2);
		//float dz =  pow(z,2);
		//float sum = dx+dy+dz;
		//distance = sqrt(sum);
		//cout << dx << "|" << dy << "|" << dz << "|" << sum << "|" << distance;

		distance = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
		switch(spectr[0]){
		case 'O':	r_color = 0.2f; g_color = 0.3f; b_color = 0.5f; break;
		case 'B':	r_color = 0.3f; g_color = 0.4f; b_color = 0.5f; break;
		case 'A':	r_color = 0.5f; g_color = 0.5f; b_color = 0.5f; break; 
		case 'F':	r_color = 0.5f; g_color = 0.5f; b_color = 0.5f; break;
		case 'G':	r_color = 0.5f; g_color = 0.5f; b_color = 0.0f; break;
		case 'K':	r_color = 0.5f; g_color = 0.4f; b_color = 0.0f; break;
		case 'M':	r_color = 0.5f; g_color = 0.2f; b_color = 0.0f; break;
		default:	r_color = 0.5f; g_color = 0.5f; b_color = 0.5f; break;
		}
		size = (1.0f/distance)*100;
	}

	friend std::ostream& operator<< (std::ostream& os, Star_spr& s){
    os << "Star_spr:" << "RA:" << s.ra << ';' << "DEC" << s.dec << ';' << "MAGN" << s.magn << ';' 
		<< "x:" << s.x << ';' << "y:" << s.y << ';' << "z:" << s.z << ';' << "Spectr:" << s.spectr << ';'
		<< "distance:" << s.distance << ';' << "size:" << s.size << ';' 
		<< "color:" << s.r_color << ';' << s.g_color << ';' << s.b_color << '\n';
    return os;
	}

};   


class vec3{
public:
	float x, y, z;

	friend std::ostream& operator<< (std::ostream& os, vec3& s){
    os << "vec3:" << "x:" << s.x << '\t' << "y:" << s.y << '\t' << "z:" << s.z << "\n";
    return os;
	}
};

class face{
public:
	unsigned int num_v;
	unsigned int num_vt;
	unsigned int num_vn;

	friend std::ostream& operator<< (std::ostream& os, face& s){
    os << "face:" << "num_v: " << s.num_v << '\t' << "num_vt: " << s.num_vt << '\t' << "num_vn: " << s.num_vn << "\n";
    return os;
	}
};

class objModel{
public:
	vector <vec3> vertices;
	vector <pair <float, float>> uvs;
	vector <vec3> normals;
	vector <vector<face>> elements;
	void loadObjModel(const char* filename) {//todo �������� e
		//std::regex reg_v("v\\s+(-*[0-9]+\\.[0-9]+)\\s+(-*[0-9]+\\.[0-9]+)\\s+(-*[0-9]+\\.[0-9]+)");
		std::regex reg_v("v\\s+(-*[0-9]+\\.[0-9]+e*-*[0-9]*)\\s+(-*[0-9]+\\.[0-9]+)\\s+(-*[0-9]+\\.[0-9]+)");
		std::regex reg_vt("vt\\s+-*([0-9]+\\.[0-9]+)\\s+-*([0-1]+\\.[0-9]+)");
		std::regex reg_vn("vn\\s+(-*[0-9]+\\.[0-9]+)\\s+(-*[0-9]+\\.[0-9]+)\\s+(-*[0-9]+\\.[0-9]+)");
		//std::regex reg_f("f\\s+(\\d+)/(\\d*)/(\\d+)\\s+(\\d+)/(\\d*)/(\\d+)\\s+(\\d+)/(\\d*)/(\\d+)");
		std::regex reg_f("f\\s+(\\d+)/(\\d*)/(\\d+)\\s+(\\d+)/(\\d*)/(\\d+)\\s+(\\d+)/(\\d*)/(\\d+)");
		std::regex reg_comment("(#.+)|(mtllib.+)|(o.+)|(s.+)|(g.+)");
		std::smatch match;

		uvs.reserve(10000);
		vertices.reserve(10000);
		normals.reserve(10000);
		elements.reserve(10000);

		ifstream in(filename, ios::in);
		if (!in){
			cerr << "Cannot open " << filename << endl; exit(1);
		}

		std::string line(
	  (std::istreambuf_iterator<char>(
		*(std::auto_ptr<std::ifstream>(
		  new std::ifstream(filename)
		)).get()
	  )), 
	  std::istreambuf_iterator<char>()
		);

		//cout << line;

		std::istringstream iss(line);

		 while (getline(iss, line))
		{
			if (std::regex_search(line, match, reg_v)){
				vec3 temp_reg_v_vec3;
				temp_reg_v_vec3.x = (float)atof(match[1].str().c_str());
				temp_reg_v_vec3.y = (float)atof(match[2].str().c_str());
				temp_reg_v_vec3.z = (float)atof(match[3].str().c_str());
				vertices.push_back(temp_reg_v_vec3);
				//cout << "reg_v found " << vertices.back() << "\n"; 
			 } 
			else if (std::regex_search(line, match, reg_vt)){
				pair <float, float> temp_pair_uvs;
				temp_pair_uvs.first =  (float)atof(match[1].str().c_str());
				temp_pair_uvs.second =  (float)atof(match[2].str().c_str());
				uvs.push_back(temp_pair_uvs);
				//cout << "reg_vt found " << uvs.back().first << "\t" << uvs.back().second << "\n"; 
			 } 
			else if (std::regex_search(line, match, reg_vn)){
				vec3 temp_reg_vn_vec3;
				temp_reg_vn_vec3.x = (float)atof(match[1].str().c_str());
				temp_reg_vn_vec3.y = (float)atof(match[2].str().c_str());
				temp_reg_vn_vec3.z = (float)atof(match[3].str().c_str());
				normals.push_back(temp_reg_vn_vec3);
				//cout << "reg_vn found " << normals.back() << "\n"; 
			 } 
			else if (std::regex_search(line, match, reg_f)){
				face face1 = {atoi(match[1].str().c_str()),atoi(match[2].str().c_str()),atoi(match[3].str().c_str())};
				face face2 = {atoi(match[4].str().c_str()),atoi(match[5].str().c_str()),atoi(match[6].str().c_str())};
				face face3 = {atoi(match[7].str().c_str()),atoi(match[8].str().c_str()),atoi(match[9].str().c_str())};
				vector <face> temp_vector_face;
				temp_vector_face.push_back(face1);
				temp_vector_face.push_back(face2);
				temp_vector_face.push_back(face3);
				elements.push_back(temp_vector_face);
				//cout  << "\n" << "reg_f found " << "\n";
				//cout <<	"face: " << get<0>(elements.back()) << "\t" <<	get<1>(elements.back()) << "\t" <<	get<2>(elements.back()) << "\t" << "\n"; 
			 } 
			else if (std::regex_search(line, match, reg_comment)){
				//cout  << "\n" << "comment " << "\n";
			 } 
			else {
				cout << "Regular expression not found for line:" << "\n" << line << "\n";
			  } 
		}
	}
	void display(float scale) {
		for(unsigned int i=0; i<elements.size();i++){
			glBegin(GL_TRIANGLES);
			for(unsigned int j=0;j<elements[i].size();j++){
				vec3 temp_vec3 = vertices[elements[i][j].num_v-1];//get vertices coord - in the obj format numbering begins with 1 in difference from a vector
				pair <float, float> temp_uv = uvs[elements[i][j].num_vt-1]; //get UVS
				vec3 temp_norm_vec3 = normals[elements[i][j].num_vn-1]; //get normal
				glTexCoord2f(temp_uv.first, temp_uv.second);
				glNormal3f(temp_norm_vec3.x*scale,temp_norm_vec3.y*scale,temp_norm_vec3.z*scale);
				glVertex3f(temp_vec3.x*scale,temp_vec3.y*scale,temp_vec3.z*scale);
			}
			glEnd();
		}

	}

};