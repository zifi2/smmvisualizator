#pragma once
#include <math.h>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <regex>
#include <tuple>

#include "freeglut.h"

/*DevIL headers*/
#include "IL/il.h"
#include "IL/ilu.h"
#include "IL/ilut.h"

#include "Star_spr.cpp"
#include "sstream" 

void fpscheck(int timerid);
void reshapeScene(int width, int height);
void processNormalKeys(unsigned char key, int x, int y);
void processSpecialKeys(int key, int x, int y);
void mouseButton(int button, int state, int x, int y) ;
void mouseMove(int x, int y);
void renderBitmapString(
		float x,
		float y,
		float z,
		void *font,
		char *string);
void loadStarCatalog();
void displayStar();
void loadTextures();
void displayScene();
void fpscheck(int timerid);
void timeIncr(int timerid);
